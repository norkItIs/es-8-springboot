package com.search_service_es.intf;

import com.search_service_es.dto.Employee;
import java.io.IOException;
import java.util.List;

public interface service_intf {

    List<Employee> get_all_data() throws IOException;

    // List<Employee> get_specific_docs(String search_string) throws IOException;
}
