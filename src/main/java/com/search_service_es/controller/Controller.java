package com.search_service_es.controller;

import com.search_service_es.dto.Employee;
import com.search_service_es.intf.service_intf;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
public class Controller {

    @Autowired
    public service_intf serv;

    @GetMapping("/example")
    public List<Employee> get_all_data() throws IOException {
        return serv.get_all_data();
    }
}
