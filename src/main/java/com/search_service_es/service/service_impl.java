package com.search_service_es.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.search_service_es.dto.Employee;
import com.search_service_es.intf.service_intf;
import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch.core.SearchResponse;

@Service
public class service_impl implements service_intf {

    @Autowired
    private ElasticsearchClient client;

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public List<Employee> get_all_data() throws IOException {
        List<Employee> employeeList = new ArrayList<>();
        final SearchResponse<Employee> search = client.search(s -> s
                .index("ttt")
                .query(q -> q
                        .term(t -> t
                                .field("text")
                                .value("one"))),
                Employee.class);
        System.out.println(search.hits().hits().toArray().length);
        search.hits().hits()
                .forEach(
                        h -> {
                            employeeList.add(
                                    objectMapper.convertValue(h.source(), Employee.class));
                        });
        return employeeList;
    }
}
