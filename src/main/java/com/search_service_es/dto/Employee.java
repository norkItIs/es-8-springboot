package com.search_service_es.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class Employee {
    @JsonProperty("text")
    @Field(name = "text", type = FieldType.Keyword)
    private String text;

    @JsonProperty("date")
    @Field(name = "date", type = FieldType.Date)
    private java.util.Date date;

    public String getText() {
        return this.text + "0";
    }
}
